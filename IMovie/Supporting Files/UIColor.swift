//
//  UIColor.swift
//  IMovie
//
//  Created by Katrin on 12.12.2021.
//

import Foundation
import UIKit

extension UIColor {
    
    static func mainWhite() -> UIColor {
        return #colorLiteral(red: 0.968627451, green: 0.9725490196, blue: 0.9921568627, alpha: 1)
    }
    
    static func mainBlack() -> UIColor {
        return #colorLiteral(red: 0.09019607843, green: 0.09411764706, blue: 0.09803921569, alpha: 1)
    }
    
    static func textFieldLight() -> UIColor {
        return #colorLiteral(red: 0.9019607843, green: 0.9019607843, blue: 0.9019607843, alpha: 1)
    }
}
