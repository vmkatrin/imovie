//
//  TableView.swift
//  IMovie
//
//  Created by Katrin on 08.12.2021.
//

import UIKit

// paralax effect
class ParalaxDetailView: UITableView {
        
    @IBOutlet weak var view: UIView!
    
    var heightConstraint: NSLayoutConstraint?
    var bottomConstraint: NSLayoutConstraint?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let header = tableHeaderView else { return }
        
        
        if heightConstraint == nil {
                heightConstraint = view.constraints.filter{ $0.identifier == "height" }.first
                bottomConstraint = header.constraints.filter{ $0.identifier == "bottom" }.first
        }
        
        //
        /// contentOffset - is a dynamic value from the top
        /// adjustedContentInset fixed valie from the top
        let offsetY = -contentOffset.y// + adjustedContentInset.top)
        //            heught.constant = max(header.bounds.height, header.bounds.height + offsetY)
        
        heightConstraint?.constant = max(header.bounds.height, header.bounds.height + offsetY)
        bottomConstraint?.constant = offsetY >= 0 ? 0 : offsetY / 2
        
        header.clipsToBounds = offsetY <= 0
    }
  
}
