//
//  CellOfMovie.swift
//  IMovie
//
//  Created by Katrin on 09.11.2021.
//

import UIKit

class CellOfMovie: UITableViewCell {

    @IBOutlet weak var imageOfMovie: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var overview: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // color settings
        backgroundColor = .mainBlack()
        title.textColor = .textFieldLight()
        rating.textColor = .textFieldLight()
        overview.textColor = .textFieldLight()
    }
}
