//
//  GenreSortedCollectionViewCell.swift
//  IMovie
//
//  Created by Katrin on 17.11.2021.
//

import UIKit

class GenreSortedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var genreImageOfMovie: UIImageView!
    @IBOutlet weak var genreSortedTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // color settings
        backgroundColor = .mainBlack()
        genreSortedTitle.textColor = .textFieldLight()
        
        // round frame
        genreImageOfMovie.layer.cornerRadius = genreImageOfMovie.frame.size.width / 2
        genreImageOfMovie.clipsToBounds = true
        genreImageOfMovie.contentMode = .scaleAspectFill
    }
}
