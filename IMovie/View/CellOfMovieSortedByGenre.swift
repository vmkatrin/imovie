//
//  CellOfMovieSortedByGenre.swift
//  IMovie
//
//  Created by Katrin on 29.11.2021.
//

import UIKit

class CellOfMovieSortedByGenre: UITableViewCell {
    
    @IBOutlet weak var imageOfMovieByGenre: UIImageView!
    @IBOutlet weak var titleOfMovieByGenre: UILabel!
    @IBOutlet weak var ratingOfMovieByGenre: UILabel!
    @IBOutlet weak var overviewOfMovieByGenre: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // color settings
        backgroundColor = .mainBlack()
        titleOfMovieByGenre.textColor = .textFieldLight()
        titleOfMovieByGenre.textColor = .textFieldLight()
        ratingOfMovieByGenre.textColor = .textFieldLight()
        overviewOfMovieByGenre.textColor = .textFieldLight()
    }
}
