//
//  CellOfMovieByDetailVC.swift
//  IMovie
//
//  Created by Katrin on 12.12.2021.
//

import UIKit

class CellOfMovieByDetailVC: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .mainBlack()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
