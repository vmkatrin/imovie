//
//  ListOfMoviesBySelectedGenreViewController.swift
//  IMovie
//
//  Created by Katrin on 29.11.2021.
//

import UIKit

class ListOfMoviesBySelectedGenreViewController: UITableViewController {
    
    // genre and name from CollectionVie MainVC
    var currentGenre: Int?
    var currentGenreName: String?

    // variables for loading data
    var imageApi = ImageLoader()
    var dataFetcherService = DataFetcherService()
   
    // arrays of data
    var moviesArray = [Movie]()
    var genresArray = [Genre]()
    
    // movie by currentGenre
    var filtredMovies = [Movie]()
    
//    @IBOutlet weak var navigationGenre: UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundColor = .mainBlack()
        
        // get movies
        dataFetcherService.fetchMovies { [weak self] (movieObjects) in
            self?.onDataReceivedMovies(movieObjects)
        }
        
        //get genres
        dataFetcherService.fetchGenre { [weak self] (genresObjects) in
            self?.onDataReceivedGenres(genresObjects)
        }
        
        // navigation title
        navigationItem.title = currentGenreName

    }

    func filtringMovies(searchGenre: Int?) {
        filtredMovies = moviesArray.filter({ (movie: Movie) -> Bool in
            return movie.genreCode.contains(searchGenre!)
        })
    }
    
    
    func onDataReceivedMovies(_ data: [Movie]) {
        moviesArray = data
      
        DispatchQueue.main.async {
            self.filtringMovies(searchGenre: self.currentGenre)
            self.tableView.reloadData()
        }
    }
    
    func onDataReceivedGenres(_ data: [Genre]) {
        genresArray = data

        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailMovieFromLissByGenre" {
            guard let indexPath = tableView.indexPathForSelectedRow else { return }
            let detailMovieVC = segue.destination as! DetailMovieTableViewController
            detailMovieVC.currentMovie = filtredMovies[indexPath.row]
        }
    }

    // MARK: - TableView Datasource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // warning Incomplete implementation, return the number of rows
        return filtredMovies.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellOFMovieByGenre", for: indexPath) as! CellOfMovieSortedByGenre
        let filtredMovie = filtredMovies[indexPath.row]
        cell.titleOfMovieByGenre.text = filtredMovie.title
        cell.ratingOfMovieByGenre.text = filtredMovie.ratingString
        cell.overviewOfMovieByGenre.text = filtredMovie.overview
        cell.imageOfMovieByGenre.image = imageApi.load(posterPath: filtredMovie.posterPath, size: ImageLoader.Size.small)
        return cell
    }
}
