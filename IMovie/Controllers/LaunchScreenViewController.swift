//
//  LaunchScreenViewController.swift
//  IMovie
//
//  Created by Katrin on 14.12.2021.
//

import UIKit

class LaunchScreenViewController: UIViewController {
   
    var replicatorlayer: CAReplicatorLayer!
    var sourceLayer: CALayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // color settings
        view.backgroundColor = .mainBlack()
        
        replicatorlayer = CAReplicatorLayer()
        sourceLayer = CALayer()
        
        self.view.layer.addSublayer(replicatorlayer)
        replicatorlayer.addSublayer(sourceLayer)
        
        startAnimation(delay: 0.1, replicates: 30)
    }
    
    override func viewWillLayoutSubviews() {
        replicatorlayer.frame = self.view.bounds
        replicatorlayer.position = self.view.center
        
        sourceLayer.frame = CGRect(x: 0.0, y: 0.0, width: 3, height: 17)
        sourceLayer.backgroundColor = UIColor.white.cgColor
        sourceLayer.position = self.view.center
        sourceLayer.anchorPoint = CGPoint(x: 0.0, y: 5.0)
    }
    
    func startAnimation(delay: TimeInterval, replicates: Int) {
        replicatorlayer.instanceCount = replicates
        
        //(2.0 * Double.pi) - 360 градусов / одинаковое расстояние(равных к-во градусов. на к-во единиц)
        let angle = CGFloat(2.0 * Double.pi) / CGFloat(replicates)
        replicatorlayer.instanceTransform = CATransform3DMakeRotation(angle, 0.0, 0.0, 1.0)
        
        replicatorlayer.instanceDelay = delay
        
        sourceLayer.opacity = 0
        let opacityAnimation = CABasicAnimation(keyPath: "opacity")
        opacityAnimation.fromValue = 1
        opacityAnimation.toValue = 0
        opacityAnimation.duration = Double(replicates) * delay
        opacityAnimation.repeatCount = Float.infinity
        
        sourceLayer.add(opacityAnimation, forKey: nil)
    }
}
