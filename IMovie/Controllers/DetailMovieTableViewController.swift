//
//  DetailMovieTableViewController.swift
//  IMovie
//
//  Created by Katrin on 11.11.2021.
//

import UIKit
import youtube_ios_player_helper

class DetailMovieTableViewController: UITableViewController {

    // movie from MainVC
    var currentMovie: Movie?
    
    var trailer: Trailer?
    
    // array of data
    var genreArray = [Genre]()
    
    // loading image
    var imageApi = ImageLoader()
    
    // trailer outlet
    @IBOutlet weak var youtubePlayer: YTPlayerView!
    
    // fetch data
    var dataFetcherService = DataFetcherService()
    
    // user interface elements
    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var detailTittle: UILabel!
    @IBOutlet weak var detailRating: UILabel!
    @IBOutlet weak var detailGenre: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var showTrailer: UIButton!
    
    var isFlipped = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // color setting
        colorSetting()
                        
        dataFetcherService.fetchGenre { (genresObjects) in
            self.onDataReceivedGenres(genresObjects)
            DispatchQueue.main.async {
                self.detailGenre.text = self.getGenreNames(from: self.currentMovie!.genreCode, genres: self.genreArray)?.joined(separator: ", ")
            }
        }
        initDataOnView()
        
        
        dataFetcherService.fetchTrailers(trailerId: currentMovie!.id) { (trailerObject) in
            self.onDataReceived(trailerObject)
            DispatchQueue.main.async {
                self.loadVideo(trailer: trailerObject)
            }
            print(trailerObject)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Make the navigation bar background clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

//         Restore the navigation bar to default
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = nil

    }
    
    @IBAction func flipOver(_ sender: UIButton) {
        isFlipped = !isFlipped
        
        let fromView = isFlipped ? detailImage : youtubePlayer
        showTrailer.isHidden = false
        
        let toView = isFlipped ? youtubePlayer : detailImage
        showTrailer.isHidden = true
        
        UIView.transition(from: fromView!, to: toView!, duration: 0.5, options: [.curveEaseOut, .transitionFlipFromLeft, .showHideTransitionViews])
    }
    
    func getGenreNames(from genreIdArray: [Int], genres: [Genre]) -> [String]? {
        var array = [String]()
        for moveGenreId in genreIdArray {
            for element in genres {
                if moveGenreId == element.id {
                    array.append(element.name)
                }
            }
        }
        
        return array
    }
    
    func onDataReceived(_ data: Trailer) {
        trailer = data
    }
    
    func loadVideo(trailer: Trailer?) {
        guard let videokey = trailer?.key else { return }
        youtubePlayer.load(withVideoId: videokey)
    }
    
    func onDataReceivedGenres(_ data: [Genre]) {
        genreArray = data
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func initDataOnView() {
        detailTittle.text = currentMovie?.title
        detailRating.text = currentMovie?.ratingString
        overview.text = currentMovie?.overview
        releaseDate.text = currentMovie?.releaseDate
        detailImage.image = imageApi.load(posterPath: currentMovie!.posterPath, size: ImageLoader.Size.medium)
    }
    
    func colorSetting() {
        tableView.backgroundColor = .mainBlack()
        
        detailTittle.textColor = .textFieldLight()
        detailRating.textColor = .textFieldLight()
        detailGenre.textColor = .textFieldLight()
        overview.textColor = .textFieldLight()
        releaseDate.textColor = .textFieldLight()
    }
    
    @IBAction func actionShowTrailer(_ sender: Any) {
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
