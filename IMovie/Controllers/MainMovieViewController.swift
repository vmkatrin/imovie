//
//  ViewController.swift
//  IMovie
//
//  Created by Katrin on 05.11.2021.
//

import UIKit
import Foundation

class MainMovieViewController: UIViewController {
    
    // user interface elements
    @IBOutlet weak var genreSortedCollectionView: UICollectionView!
    @IBOutlet weak var listOfMovie: UITableView!
        
    // variables for loading data
    var imageApi = ImageLoader()
    var dataFetcherService = DataFetcherService()
    
    // array of data
    var moviesArray = [Movie]()
    var genresArray = [Genre]()
    
    // dict of downloaded poster path of movie(value) by genre(key) dictionary
    var moviesPosterByGenre = [Int: String]()
   
    // work with SearchController
    let searchController = UISearchController(searchResultsController: nil)
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    // movies from SearchController
    private var filtredMoviesArray = [Movie]()
    var isFiltering: Bool {
      return searchController.isActive && !searchBarIsEmpty
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // color settings
        view.backgroundColor = .mainBlack()
        genreSortedCollectionView.backgroundColor = .mainBlack()
        listOfMovie.backgroundColor = .mainBlack()
        
//        // get movies
//        dataFetcherService.fetchMovies { (movieObjects) in
//            self.onDataReceivedMovies(movieObjects)
//        }
//
//        // get genres
//        dataFetcherService.fetchGenre { (genresObjects) in
//            self.onDataReceivedGenres(genresObjects)
//        }
//
        
        group.enter()
        
        dataFetcherService.fetchMovies { (movieObjects) in
            self.onDataReceivedMovies(movieObjects)
        }
        
        group.enter()
        
        dataFetcherService.fetchGenre { (genresObjects) in
            self.onDataReceivedGenres(genresObjects)
        }
        
        group.notify(queue: .main) {
            DispatchQueue.main.async { [self] in
                getImageFromGenre(from: moviesArray, genres: genresArray)
                print("movies poster by genre: \(moviesPosterByGenre.count)")
                
                print("notifying !!!")
                self.listOfMovie.reloadData()
                self.genreSortedCollectionView.reloadData()
            }
        }
        
        genreSortedCollectionView.delegate = self
        genreSortedCollectionView.dataSource = self
        
        listOfMovie.delegate = self
        listOfMovie.dataSource = self
        
        setupNavigationController()
        setupSearchBar()
        
    }
    
    func setupNavigationController() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = .mainWhite()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.mainWhite()]
        navigationItem.title = "Trending Movie"
        navigationController?.navigationBar.barStyle = .black
    }

    func setupSearchBar() {
        searchController.searchBar.sizeToFit()
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        navigationItem.hidesSearchBarWhenScrolling = false
        
        searchController.searchBar.delegate = self
        
        showSearchBarButton(shouldShow: true)
    }
    
    @objc func showSearchBar() {
        search(shouldShow: true)
        searchController.becomeFirstResponder()
    }
    
    func showSearchBarButton(shouldShow: Bool) {
        if shouldShow {
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(showSearchBar))
        } else {
            navigationItem.rightBarButtonItem = nil
        }
    }
    
    func search(shouldShow: Bool) {
        showSearchBarButton(shouldShow: !shouldShow)
        searchController.searchBar.showsCancelButton = shouldShow
        navigationItem.searchController = shouldShow ? searchController : nil
    }

    let group = DispatchGroup()
    func onDataReceivedMovies(_ data: [Movie]) {
        moviesArray = data
        
        group.leave()
        
        DispatchQueue.main.async {
            self.listOfMovie.reloadData()
        }
    }
    
    func onDataReceivedGenres(_ data: [Genre]) {
        genresArray = data
        
        group.leave()
        
        DispatchQueue.main.async {
            self.getImageFromGenre(from: self.moviesArray, genres: self.genresArray)
            self.genreSortedCollectionView.reloadData()
        }
    }
    
    // array of used movies for func getImageFromGenre
    var arrayUsedMoves = [Int]()
   
    // filling out dictionary moviesPosterByGenre
    func getImageFromGenre(from movies: [Movie], genres: [Genre]) {
        var i = 0
        for genre in genres {
            if moviesPosterByGenre.keys.contains(genre.id) {
                print("undefined")
            } else {
                for movie in movies {
                    if !arrayUsedMoves.contains(movie.id) && movie.genreCode.contains(genre.id) {
                        i += 1

                        arrayUsedMoves.append(movie.id)
                        moviesPosterByGenre.updateValue(movie.posterPath, forKey: genre.id)
                        break
                    }
                }
            }
        }
    }

    func configureCell(cell: CellOfMovie, for indexPath: IndexPath) {
        let movie: Movie
        if isFiltering {
            movie = filtredMoviesArray[indexPath.row]
        } else{
            movie = moviesArray[indexPath.row]
        }
        
        cell.title.text = movie.title
        cell.rating.text = movie.ratingString
        cell.overview.text = movie.overview
        cell.imageOfMovie.image = imageApi.load(posterPath: movie.posterPath, size: ImageLoader.Size.small)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailMovieFromMainVC" {
            // delegate didSelectedRow
            guard let indexPath = listOfMovie.indexPathForSelectedRow else { return }
            let movie: Movie
            if isFiltering {
                movie = filtredMoviesArray[indexPath.row]
            } else{
                movie = moviesArray[indexPath.row]
            }
            
            let detailMovieVC = segue.destination as! DetailMovieTableViewController
            detailMovieVC.currentMovie = movie
            
        }
        else if segue.identifier == "listOfMoviesBytSelectedGenre" {
            if let cell = sender as? UICollectionViewCell,
               let indexPath = self.genreSortedCollectionView.indexPath(for: cell) {
                let vc = segue.destination as! ListOfMoviesBySelectedGenreViewController //Cast with your DestinationController
                let idOfcurrenGenre = Array(moviesPosterByGenre.keys)[indexPath.item]
                vc.currentGenreName = genresArray.first{ $0.id == idOfcurrenGenre }?.name
                vc.currentGenre = idOfcurrenGenre
            }
        }
    }
    
    @IBAction func cancelAction(_ segue: UIStoryboardSegue) {}
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension MainMovieViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filtredMoviesArray.count
        }
        return moviesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! CellOfMovie
        configureCell(cell: cell, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension MainMovieViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return moviesPosterByGenre.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "genreCell", for: indexPath) as! GenreSortedCollectionViewCell
        let moviePosterPathValue = Array(moviesPosterByGenre.values)[indexPath.item]
        
        let movieGenreKey = Array(moviesPosterByGenre.keys)[indexPath.item]
        
        for genre in genresArray {
            if genre.id == movieGenreKey {
                cell.genreSortedTitle.text = genre.name
            }
        }
        cell.genreImageOfMovie.image = imageApi.load(posterPath: moviePosterPathValue, size: ImageLoader.Size.small)
        return cell
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension MainMovieViewController: UICollectionViewDelegateFlowLayout {
    // cell`s size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .clear
        return CGSize(width: 80, height: 100)
    }
    
    // inset For Section
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
    }
}

// MARK: - UISearchResultsUpdating
extension MainMovieViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterMovieForSearchtext(searchController.searchBar.text!)
    }

    func filterMovieForSearchtext(_ searchText: String) {
        filtredMoviesArray = moviesArray.filter({ (movie: Movie) -> Bool in
            return movie.title.lowercased().contains(searchText.lowercased())
        })
        listOfMovie.reloadData()
    }
}

// MARK: - UISearchBarDelegate
extension MainMovieViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        search(shouldShow: false)
    }
}


