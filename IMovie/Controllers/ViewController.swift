//
//  ViewController.swift
//  IMovie
//
//  Created by Katrin on 05.11.2021.
//

import UIKit
import Foundation

class MainMovieViewController: UITableViewController {
    

    var networkManager = NetworkManager()
    var imageApi = ImageAPI()
    
    //let array = ["1","2","3"]
    var moviesArray = [Movie]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        networkManager.delegate = self
        networkManager.onCompletion = { [weak self] movieObjects in
            self!.onDataReceived(movieObjects)
        }
        networkManager.fetchMovies()
        
    }
    
    func onDataReceived(_ data: [Movie])
    {
        print("RECEIVED FROM CALLBACK")
        print("ready")
        moviesArray = data
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
//        print(data)
    }

    func configureCell(cell: CellOfMovie, for indexPath: IndexPath) {
        let movie = moviesArray[indexPath.row]
        
        cell.title.text = movie.title
        cell.rating.text = movie.ratingString
        cell.overview.text = movie.overview
        cell.imageOfMovie.image = imageApi.load(posterPath: movie.posterPath, size: ImageAPI.Size.small)

    }
    
    // MARK: - Table View Data Source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("get count \(moviesArray.count)")
        return moviesArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("display cell \(indexPath.row)")
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CellOfMovie
        configureCell(cell: cell, for: indexPath)
        return cell
    }
    
    // MARK: - Table View Delegate
    
//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 250
//    }
    
   
}

extension MainMovieViewController: NetworkManagerDelegate {
    func updateInterface(_: NetworkManager, with movie: [Movie]) {
        print("RECEIVED FROM DELEGATE")
    }
}


