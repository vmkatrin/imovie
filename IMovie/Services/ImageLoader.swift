//
//  ImageAPI.swift
//  IMovie
//
//  Created by Katrin on 10.11.2021.
//

import UIKit
import Foundation

class ImageLoader {
    
    static let basePath = "https://image.tmdb.org/t/p/"
    
    enum Size: String {
        case small  = "w154"
        case medium = "w500"
        case large  = "w780"
        case original = "original"
        
        func path(poster: String?) -> URL? {
            return poster != nil
                ? URL(string: (basePath + rawValue))!.appendingPathComponent(poster!)
                : nil
        }
    }
    
    func load(posterPath: String, size: Size) -> UIImage? {
        guard let imgeURL = size.path(poster: posterPath) else { return nil }
        guard let imageData = try? Data(contentsOf: imgeURL) else { return nil }
        return UIImage(data: imageData)
    }
}
