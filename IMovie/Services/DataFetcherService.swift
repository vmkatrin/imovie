//
//  DataFetcherService.swift
//  IMovie
//
//  Created by Katrin on 06.12.2021.
//

import Foundation

class DataFetcherService {
    var networkDataFetcher: NetworkDataFetcher!
    
    init(networkDataFetcher: NetworkDataFetcher = NetworkDataFetcher()) {
        self.networkDataFetcher = networkDataFetcher
    }
    
    func fetchMovies(completion: @escaping ([Movie]) -> Void) {
        let urlMovies = "https://api.themoviedb.org/3/trending/movie/day?api_key=\(apiKey)"
        networkDataFetcher.fetchGenericJSONData(urlString: urlMovies) { (data: MovieData?) in
            guard let movieData = data else {
                print("DataFetcherService: couldn't handle movie response")
                return
            }
            
            let movies = movieData.results.map { Movie(movieData: $0) }

            completion(movies)
        }
    }
    
    func fetchGenre(completion: @escaping ([Genre]) -> Void) {
        let genreURL = "https://api.themoviedb.org/3/genre/movie/list?api_key=\(apiKey)&language=en-US"
        
        networkDataFetcher.fetchGenericJSONData(urlString: genreURL) { (data: GenreData?) in
            
            guard let genreData = data else {
                print("DataFetcherService: couldn't handle genre response")
                return
                
            }
            
            let genres = genreData.genres.map { Genre(genreData: $0) }
            
            completion(genres)
        }
    }
 
    func fetchTrailers(trailerId: Int, completion: @escaping (Trailer) -> Void) {
        let trailerURL = "https://api.themoviedb.org/3/movie/\(trailerId)/videos?api_key=\(apiKey)&language=en-US"
        networkDataFetcher.fetchGenericJSONData(urlString: trailerURL) { (data: TrailerData?) in
            guard let trailerData = data else {
                print("DataFetcherService: couldn't handle trailer response")
                return
            }
            
            guard let trailer = Trailer(trailerData: trailerData) else { return }
            
            completion(trailer)
        }
    }
}
