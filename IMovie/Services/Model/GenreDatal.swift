//
//  GenreModel.swift
//  IMovie
//
//  Created by Katrin on 12.11.2021.
//

import Foundation

struct GenreData: Decodable {
    let genres: [GenreElement]
}

struct GenreElement: Decodable {
    let id: Int
    let name: String
}


