//
//  MovieData.swift
//  IMovie
//
//  Created by Katrin on 08.11.2021.
//

import Foundation

struct MovieData: Decodable {
    let results: [Results]
}

struct Results: Decodable {
    let title: String
    let voteAverage: Double
    let overview: String
    let releaseDate: String
    let genreID: [Int]
    let posterPath: String?
    let id: Int
    
    enum CodingKeys: String, CodingKey {
        case title
        case voteAverage = "vote_average"
        case overview
        case releaseDate = "release_date"
        case genreID = "genre_ids"
        case posterPath = "poster_path"
        case id
    }
}
