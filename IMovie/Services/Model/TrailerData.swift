//
//  VideoData.swift
//  IMovie
//
//  Created by Katrin on 30.11.2021.
//

import Foundation

struct TrailerData: Decodable {
    let id: Int
    let results: [ResultsOfTrailer]
}

struct ResultsOfTrailer: Decodable {
    let name: String
    let key: String
    let site: String
}
