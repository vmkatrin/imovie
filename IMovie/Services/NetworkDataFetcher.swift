//
//  NetworkDataFetcher.swift
//  IMovie
//
//  Created by Katrin on 06.12.2021.
//

import Foundation

class NetworkDataFetcher {
    
    var networkService: NetworkService!
    
    // automatic initailizasiot
    init(networkService: NetworkService = NetworkService()) {
        self.networkService = networkService
    }
    
    // connected func between data request (networkService.request) and decode data
    func fetchGenericJSONData<T: Decodable>(urlString: String, response: @escaping (T?) -> Void) {
        print("print \(T.self)")
        networkService.request(urlString: urlString) { (data, error) in
            if let error = error {
                print("Error received requesting data: \(error.localizedDescription)")
                response(nil)
            }

            let decoded = self.decodeJSON(type: T.self, from: data)
            response(decoded)

        }
    }
    
    // decode JSON data to any model
    func decodeJSON<T: Decodable>(type: T.Type, from: Data?) -> T? {
        let decoder = JSONDecoder()
        guard let data = from else { return nil}
        do {
            let objects = try decoder.decode(type.self, from: data)
            return objects
        } catch let jsonError as NSError {
            print("Failed to decode JSON", jsonError.localizedDescription)
            return nil
        }
    }
}
