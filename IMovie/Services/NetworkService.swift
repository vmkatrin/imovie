//
//  NetworkService.swift
//  IMovie
//
//  Created by Katrin on 03.12.2021.
//

import Foundation

class NetworkService {

    // building request of data by url
    func request(urlString: String, completion: @escaping (Data?, Error?) -> Void) {
        guard let url = URL(string: urlString)
        else {
            return }
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                print("get data")
                completion(data, error)
            }
        }
        task.resume()
    }
    
    
}
