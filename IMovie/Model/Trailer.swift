//
//  Video.swift
//  IMovie
//
//  Created by Katrin on 30.11.2021.
//

import Foundation

// create a model of trailer object for display on the screen
struct Trailer {
    let trailerId: Int
    let titleOfMovie: String
    let key: String
    let site: String
    
    init?(trailerData: TrailerData) {
        trailerId = trailerData.id
        guard let elementOfOficialTrailer = trailerData.results.first(where: { $0.name == "Official Trailer"} ) else { return nil }
        titleOfMovie = elementOfOficialTrailer.name
        key = elementOfOficialTrailer.key
        site = elementOfOficialTrailer.site

    }
}
