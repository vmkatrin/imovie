//
//  Genre.swift
//  IMovie
//
//  Created by Katrin on 24.11.2021.
//

import Foundation

// create a model of genre object for display on the screen
struct Genre {
    let id: Int
    let name: String
    
    init(genreData: GenreElement) {
        id = genreData.id
        name = genreData.name
    }
}
