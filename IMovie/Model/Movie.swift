//
//  Movie.swift
//  IMovie
//
//  Created by Katrin on 08.11.2021.
//

import Foundation

// create a model of movie object for display on the screen
struct Movie {
    
    let title: String
    
    let rating: Double
    var ratingString: String {
        return "\(rating)"
    }
    
    let overview: String
    let releaseDate: String
    let genreCode: [Int]
    let posterPath: String
    let id: Int
    
    init(movieData: Results) {
        func convertStringFromReleaseDate(originalDate: String) -> String? {
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            guard let date = dateFormatter.date(from: originalDate) else { return nil }
            
            dateFormatter.dateStyle = .long
            dateFormatter.timeStyle = .none
            dateFormatter.locale = Locale(identifier: "en_GB")
            let valueDate = dateFormatter.string(from: date)
            return valueDate
        }
        
        title = movieData.title
        rating = movieData.voteAverage
        overview = movieData.overview
        releaseDate = convertStringFromReleaseDate(originalDate: movieData.releaseDate) ?? "not available"
        genreCode = movieData.genreID
        posterPath = movieData.posterPath ?? ""
        id = movieData.id
        
    }
}
