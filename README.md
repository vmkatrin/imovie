# Overview

In application you can see list of most trending movies. Search for desired movie, sort by genre.
You can view the movie description, poster, rating, movie release date and trailer.
Data is taken from resource https://api.themoviedb.org

## Technology

**1.HTTP/REST API, JSON:** 

* make request URLSession
* decode JSON data to any model with JSONDecoder

**2.UIKit:** 

* UITableView (UITableViewDataSource, UITableViewDelegate)
* UICollectionView (UICollectionViewDelegate, UICollectionViewDataSource)
* UISearchController 
* UIImageView
* UILabel
* UIButton
* Storyboard
* UIStoryboardSegue

**3.MVC architecture** 

**4.GCD (download and displaying data on the screen)** 

**5.NSLayoutConstraint (make a paralax effect of movie`s poster when scrolling)**

**6.Foundation** 

**7.Library YTPlayerView with CocoaPods**

## Screenshots

**Launch Screen** 

![LaunchScreen](LaunchScreen.png)

**Main Screen** 

![MainScreen](MainScreen.png)

**Search movie** 

![Search](Search.png)

**Detail of movie** 

![Detail](Detail.png)

**Paralax effect** 

![ParalaxEffect](ParalaxEffect.png)

**Watch trailer** 

![Trailer](Trailer.png)

**Sorted movies by genre** 

![MoviesByGenre](MoviesByGenre.png)

